﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace A2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool print;                                                                                     //varibles print for selection sort prints the arrays
            string MoistData ="C:/Users/bunce/Desktop/ggg/10011594-6211-assesmment2/Moisture_Data.txt";     // the paths for the files
            string DateData = "C:/Users/bunce/Desktop/ggg/10011594-6211-assesmment2/DateTime_Data.txt"; 

            int input = 0;
            while (input != 5)
            {
                Console.WriteLine("\nPlease enter the Number of the question to cheak");  //user to input a number in a while loop
                Console.WriteLine("1) FindMaximum");
                Console.WriteLine("2) SelectionSort");
                Console.WriteLine("3) CompareArray");
                Console.WriteLine("4) BubbleSort Times");
                Console.WriteLine("and enter 5 to quit");
                input = Convert.ToInt32(Console.ReadLine());
                Console.Clear();
                if (input == 1)
                {
                    FindMaximum(MoistData);
                }
                else if (input == 2)
                {
                    SelectionSort(MoistData, print = true);
                }
                else if (input == 3)
                {
                    CompareArray(MoistData, DateData, print = false);
                }
                else if (input == 4)
                {
                    BubbleSortTimeing.BubbleSort(MoistData);
                    BubbleSortTimeing.ImprovedBubbleSort(MoistData);
                    Console.WriteLine("\npress any key to return to menu");
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }
        public static void PrintArray(double[] x)
        {
            foreach (Double g in x)
            {
                Console.Write("{0}, ", g);
            }
        }
        public static string[] ImportData(string path) 
        {
            string[] original = System.IO.File.ReadAllLines(path);          //reads the path into a string array
            return original;
        }

        public static void FindMaximum(string MoistData) 
        {
            string path = MoistData;
            double[] max = Array.ConvertAll<string, double>(ImportData(path), double.Parse); //clones the moisture data to a new array
            List<double> usernmax = new List<double>();
            Console.WriteLine("please enter a Number to see the that number of the largest moisture data");
            int usern = Convert.ToInt32(Console.ReadLine());                                                


            for (int n = 0; n < usern; n++)                                                 // user input for the biggest numbers
            {
                int index = 0;
                double l = max[0];
                for (int i = 0; i < max.Length; i++)                                        //through the array if the number is bigger than the last swaps untill the largest one
                {                                                                          
                    if (max[i] > l)
                    {
                        l = max[i];
                        index = i;
                    }
                }
                usernmax.Add(l);
                max.SetValue(0, index);                                                     // adds the biggest number, then remove from the array so that number dosn't get picked up again
            }
            max = usernmax.ToArray();
            Console.WriteLine("The orginal array");
            double[] x = Array.ConvertAll<string, double>(ImportData(path), double.Parse);  // displaying the array and input number of largest numbers
            PrintArray(x);
            Console.WriteLine("\nThe {0} biggent numbers of the Data", usern);
            PrintArray(x = max.Clone() as double[]);
            Console.WriteLine("\npress any key to return to menu");
            Console.ReadKey();
            Console.Clear();
        }
        public static Dictionary<int, double> SelectionSort(string MoistData, Boolean print)
        {
            string path = MoistData;
            double[] sort = Array.ConvertAll<string, double>(ImportData(path), double.Parse); 
            int min;
            double temp;
            for (int i = 0; i < sort.Length; i++)                       
            {
                min = i;
                for (int j = i + 1; j < sort.Length; j++)           //uses the two loops selects two neighboring varibles of the array
                {
                    if (sort[j] < sort[min])                        //if it is smaller it swaps them around
                    {
                        min = j;
                    }
                    if (min != 1)                                   
                    {
                        temp = sort[i];
                        sort[i] = sort[min];
                        sort[min] = temp;
                    }
                }
            }
            Dictionary<int, double> BSA = new Dictionary<int, double>();    //dictionary for the biggest, smallest and average
            double av = Math.Round(sort.Average(),2);                       //finding the average
            if (print == true)                                              //if print bool == true it prints the arrays
            {
                double[] x = Array.ConvertAll<string, double>(ImportData(path), double.Parse);
                PrintArray(x);
                PrintArray(x = sort.Clone() as double[]);
                Console.WriteLine("\nThe minimum {0}, the Average {1}, The Maximum {2}", sort[0], av, sort[sort.Length - 1]);
                Console.WriteLine("\npress any key to return to menu");
                Console.ReadKey();
                Console.Clear();
            }

            BSA.Add(1, sort[sort.Length - 1]); //adds the right data to dictionary
            BSA.Add(2, sort[0]);
            BSA.Add(3, av);

            return BSA;
        }
        public static Dictionary<int, double> LinearSearch(string MoistData, Boolean print)
        {
            string path = MoistData;
            double[] LS = Array.ConvertAll<string, double>(ImportData(path), double.Parse);
            Dictionary<int, double> BSAI = SelectionSort(MoistData, print = false);             //imports the array and dictionary
            for (int i = 0; i < LS.Length; i++)                                                 // loops throught the array, compares against the dictonary
            {                                                                                   
                if (LS[i] == BSAI[1])
                {
                    BSAI.Remove(1);                                                             //when array matches the dictionary removes the dictionary data and makes a new imput of the array index
                    BSAI.Add(1, i);
                }
                else if (LS[i] == BSAI[2])
                {
                    BSAI.Remove(2);
                    BSAI.Add(2, i);
                }
                else if (LS[i] == BSAI[3])
                {
                    BSAI.Remove(3);
                    BSAI.Add(3, i);
                }
            }
            return BSAI;
        }

        public static void CompareArray(string MoistData, string DateData, Boolean print)
        {
            string path = DateData;
            string[] CA = ImportData(path).Clone() as string[];
            Dictionary<int, double> BSADATE = LinearSearch(MoistData, print = false);               //imports the correct data then displays it
            Dictionary<int, double> BSAI = SelectionSort(MoistData, print = false);

            Console.WriteLine("This is the Maximum moisture {0} and it's date {1}", BSAI[1], CA[Convert.ToInt32(BSADATE[1])]);
            Console.WriteLine("This is the Minimum moisture {0} and it's date {1}", BSAI[2], CA[Convert.ToInt32(BSADATE[2])]);
            Console.WriteLine("This is the Average moisture {0} and it's date {1}", BSAI[3], CA[Convert.ToInt32(BSADATE[3])]);
            Console.WriteLine("\npress any key to return to menu");
            Console.ReadKey();
            Console.Clear();
        }
    }
    class BubbleSortTimeing
    {
        public static void BubbleSort(string MoistData)
        {
            string path = MoistData;                    
            Stopwatch BS = new Stopwatch();                                                                 
            double[] bubble = Array.ConvertAll<string, double>(Program.ImportData(path), double.Parse);
            double temp;
            BS.Start();                                             //start the stopwatch
            for (int i = 0; i < bubble.Length; i++)                 //loops the array to move the largest number to the end
            {
                for (int j = 0; j < bubble.Length - 1; j++)     
                {
                    if (bubble[j] > bubble[j + 1])
                    {
                        temp = bubble[j + 1];
                        bubble[j + 1] = bubble[j];
                        bubble[j] = temp;
                    }
                }
            }
            BS.Stop();
            Console.WriteLine("The time of the bubble sort is {0}", BS.Elapsed);
        }

        public static void ImprovedBubbleSort(string MoistData)
        {
            string path = MoistData;
            Stopwatch IB = new Stopwatch();
            double[] Impbubble = Array.ConvertAll<string, double>(Program.ImportData(path), double.Parse);  
            double temp;
            IB.Start();                                                 
            for (int i = 0; i < Impbubble.Length; i++)                  //loop the array then move the largest number to the end
            {
                int check = 0;
                for (int j = 0; j < Impbubble.Length - 1 - i; j++)      //but when looping to compare the two values it use the amount of times that it's moved the largest value to the end "i"
                {                                                       //to loops to the last moved number insted of looping through the entire array
                    if (Impbubble[j] > Impbubble[j + 1])
                    {
                        temp = Impbubble[j + 1];
                        Impbubble[j + 1] = Impbubble[j];
                        Impbubble[j] = temp;
                        check++;
                    }
                }
                if (check == 0)                                         //if "check" int to see if any swaps have been done if not the array is ordered and break
                {
                    break;
                }
            }
            IB.Stop();
            Console.WriteLine("The time of the improved bubble sort is {0}", IB.Elapsed);
        }
    }
}